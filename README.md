#QQRCode

### This sample code written in Swift is part of [Quaddro Swift Bootcamp](http://swift.quaddro.com.br/ "Title") and shows how to read a QR Code using AVFoundation.


* [Also visit our blog: Swift Playground](http://swiftplayground.com.br/ "Title")

```swift
//
//  ViewController.swift
//  QR Code Reader
//
//  Created by Danilo Altheman on 04/11/14.
//  Copyright (c) 2014 Danilo Altheman - © Quaddro www.quaddro.com.br. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    let captureSession: AVCaptureSession = AVCaptureSession()
    var captureVideoPreviewLayer: AVCaptureVideoPreviewLayer!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelVisualEffectView: UIVisualEffectView!
    @IBOutlet weak var QRCodeLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        captureVideoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        startReading()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Custom Methods
    func startReading() -> Bool {
        var error: NSError?
        let captureDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        let captureDeviceInput = AVCaptureDeviceInput(device: captureDevice, error: &error)
        if captureDeviceInput == nil {
            println("Error \(error)")
            return false
        }
        captureSession.addInput(captureDeviceInput)
        let captureMetadataOutput = AVCaptureMetadataOutput()
        captureSession.addOutput(captureMetadataOutput)
        
        let queue = dispatch_queue_create("video_queue", nil)
        captureMetadataOutput.setMetadataObjectsDelegate(self, queue: queue)
        captureMetadataOutput.metadataObjectTypes = [AVMetadataObjectTypeQRCode]
        
        captureVideoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        captureVideoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill

        imageView.layer.addSublayer(captureVideoPreviewLayer)
        captureSession.startRunning()
        return true
    }
    
    override func viewDidLayoutSubviews() {
        captureVideoPreviewLayer.frame = imageView.layer.bounds
    }
    
    // MARK: - AVCaptureMetadataOutputObjectsDelegate
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [AnyObject]!, fromConnection connection: AVCaptureConnection!) {
        println("Capture output")
        if metadataObjects != nil && metadataObjects.count > 0 {
            let metadataMachineReadableCodeObject: AVMetadataMachineReadableCodeObject = metadataObjects[0] as AVMetadataMachineReadableCodeObject
            if metadataMachineReadableCodeObject.type == AVMetadataObjectTypeQRCode {
                println(metadataMachineReadableCodeObject.stringValue)
                dispatch_async(dispatch_get_main_queue()) {
                    () -> Void in
                    UIView.animateWithDuration(0.2, animations: { () -> Void in
                        self.QRCodeLabel.text = metadataMachineReadableCodeObject.stringValue
                        self.labelVisualEffectView.alpha = 1.0
                    })
                }
            }
        }
        else {
            dispatch_async(dispatch_get_main_queue()) {
                () -> Void in
                UIView.animateWithDuration(0.2, animations: { () -> Void in
                    self.QRCodeLabel.text = ""
                    self.labelVisualEffectView.alpha = 0.0
                })
            }
        }
    }
}


```